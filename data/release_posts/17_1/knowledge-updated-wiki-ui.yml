---
features:
  primary:
  - name: "Improved wiki user experience"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/wiki/'
    video: 'https://www.youtube-nocookie.com/embed/t2z7sZoJ6oE'
    reporter: mmacfarlane
    stage: plan
    categories:
     - Wiki
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/452225'
    description: |
     The wiki feature in GitLab 17.1 provides a more unified and efficient workflow:

     - [Easier and quicker cloning](https://gitlab.com/gitlab-org/gitlab/-/issues/281830) with a new repository clone button. This improves collaboration, and speeds up access to the wiki content for editing or viewing. 
     - [A more obvious delete option](https://gitlab.com/gitlab-org/gitlab/-/issues/335169) in a more discoverable location. This reduces the time spent searching for it, and minimizes potential errors or confusion when managing wiki pages. 
     - [Allowing empty pages to be valid](https://gitlab.com/gitlab-org/gitlab/-/issues/221061), improving flexibility. Create empty placeholders when you need them. Focus on better planning and organization of wiki content, and fill in the empty pages later.
     
     These enhancements improve ease of use, discoverability, and content management in your wiki's workflow. We want your wiki experience to be efficient and user-friendly. By making cloning repositories more accessible, relocating key options for better visibility, and allowing for the creation of empty placeholders, we're refining our platform to better meet your users' needs.
