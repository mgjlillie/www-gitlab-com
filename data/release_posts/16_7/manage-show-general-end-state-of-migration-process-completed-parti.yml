---
features:
  secondary:
  - name: "Comprehensive results of imports by direct transfer"
    available_in:  [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/group/import/#review-results-of-the-import'
    reporter: m_frankiewicz
    stage: manage
    categories:
    - Importers
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/394727'
    description: |
       Knowing how crucial for our users is to understand the results of the import process, in this milestone we further improved on information presented for imports by
       direct transfer. We now display import status badges next to GitLab groups and projects on:

       - The [page where you can select groups and projects to import](https://docs.gitlab.com/ee/user/group/import/index.html#select-the-groups-and-projects-to-import).
       - The [page listing imported groups and projects](https://docs.gitlab.com/ee/user/group/import/index.html#group-import-history).

       The import status badges are:

       - **Not started**
       - **Pending**
       - **Importing**
       - **Failed**
       - **Timeout**
       - **Cancelled**
       - **Complete**
       - **Partially completed**

       The **Partially completed badge** was added in this release and identifies a completed import process that has some items (such as merge requests or issues) not imported. 

       Groups that an import process was started for have a **View details** link that shows imported subgroups and projects for that particular group. From there, you can see
       the list of items that couldn't be imported (if any) by clicking a **See failures** link. **See failures** was
       [released in the last release](https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#comprehensive-list-of-items-that-failed-to-be-imported).
      
       In this milestone we also improved navigation with the breadcrumbs between those pages.

      
