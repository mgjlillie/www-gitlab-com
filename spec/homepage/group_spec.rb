describe Gitlab::Homepage::Group do
  subject(:group) { described_class.new(key, data) }
  let(:key) { 'source_code' }
  let(:categories) { %w[foo bar baz] }
  let(:data) do
    {
      'name' => 'Source Code',
      'categories' => categories,
      'slack' => { 'channel' => 'g_create_source-code' }
    }
  end

  before do
    allow(YAML).to receive(:load_file).and_call_original
  end

  describe '#method_missing' do
    it 'returns value by key' do
      expect(group.name).to eq('Source Code')
    end

    context 'when key is missing' do
      it { expect(group.unknown).to be_nil }
    end
  end

  describe '#categories' do
    subject { group.categories }

    it { is_expected.to eq(%w[foo bar baz]) }

    context 'when categories is nil' do
      let(:categories) { nil }

      it { is_expected.to eq([]) }
    end
  end

  describe '#label' do
    subject { group.label }

    context 'when name is defined' do
      it { is_expected.to eq("#{described_class::LABEL_PREFIX}source code") }
    end

    context 'when label is defined' do
      let(:label_name) { 'group::certify' }
      let(:data) { { 'label' => label_name } }

      it { is_expected.to eq(label_name) }
    end
  end

  describe '#extra_labels' do
    subject { group.extra_labels }

    context 'when extra_labels is defined' do
      let(:extra_labels_names) { %w[foo bar] }
      let(:data) { { 'extra_labels' => extra_labels_names } }

      it { is_expected.to eq(extra_labels_names) }
    end

    context 'when extra_labels is not defined' do
      it { is_expected.to eq([]) }
    end
  end

  describe '#slack' do
    subject { group.slack }

    context 'when slack.channel is defined' do
      it { is_expected.to eq('g_create_source-code') }
    end

    context 'when slack is not defined' do
      let(:data) { {} }

      it { is_expected.to be_nil }
    end

    context 'when slack.channel is not defined' do
      let(:data) { { 'slack' => {} } }

      it { is_expected.to be_nil }
    end
  end

  describe '.all!' do
    subject(:groups) { described_class.all! }

    before do
      allow(YAML).to receive(:load_file).with(File.expand_path('../../data/stages.yml', __dir__)) do
        {
          'stages' => {
            'create' => { 'groups' => { 'source_code' => { 'name' => 'Source Code' } } },
            'plan' => { 'groups' => { 'project_management' => { 'name' => 'Project Management' } } }
          }
        }
      end
    end

    around do |test|
      described_class.reset_all!
      test.run
      described_class.reset_all!
    end

    it 'returns Group objects' do
      expect(groups.count).to eq(2)
      expect(groups.map(&:name)).to match_array(['Source Code', 'Project Management'])
    end
  end

  describe '.ssot' do
    subject(:groups) { described_class.ssot }

    before do
      allow(YAML).to receive(:load_file).with(File.expand_path('../../data/stages.yml', __dir__)) do
        {
          'stages' => {
            'create' => { 'groups' => { 'source_code' => { 'name' => 'Source Code', 'pm' => 'TBD' } } },
            'plan' => { 'groups' => { 'project_management' => { 'name' => 'Project Management', 'pm' => ['TBD'] } } }
          }
        }
      end
    end

    around do |test|
      described_class.reset_all!
      test.run
      described_class.reset_all!
    end

    it 'returns SSOT object and excludes "TBD" people' do
      expect(groups.count).to eq(2)
      expect(groups.dig(:source_code, :product_managers)).to eq([])
      expect(groups.dig(:project_management, :product_managers)).to eq([])
    end
  end
end
