---
layout: sec_direction
title: "Category Direction - Static Application Security Testing (SAST)"
description: "Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities."
canonical_path: "/direction/secure/static-analysis/sast/"
---

- TOC
{:toc}

## SAST

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-05-06` |

<!-- Note: This document includes TODO comments. Publishing the updated content with TODOs remaining is a way to share the overall direction earlier and more iteratively, rather than waiting for all of the content requested by the new template to be ready to publish. -->

### Introduction and how you can help
This direction page describes GitLab's plans for the SAST category, which checks source code to find possible security vulnerabilities.

This page is maintained by the Product Manager for [Static Analysis](/handbook/product/categories/#static-analysis-group), [Connor Gilbert](/company/team/#connorgilbert).

Everyone can contribute to where GitLab SAST goes next, and we'd love to hear from you.
The best ways to participate in the conversation are to:
- Comment or contribute to existing [SAST issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3ASAST) in the public `gitlab-org/gitlab` issue tracker.
- If you don't see an issue that matches, file [a new issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20Proposal%20-%20basic).
    - Post a comment that says `@gitlab-bot label ~"group::static analysis" ~"Category:SAST"` so your issue lands in our triage workflow.
- If you're a GitLab customer, discuss your needs with your account team.

### Overview

[GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/) runs on merge requests and the default branch of your software projects so you can continuously monitor and improve the security of the code you write.
SAST jobs run in your CI/CD pipelines alongside existing builds, tests, and deployments, so it's easy for developers to interact with.

While SAST uses sophisticated techniques, we want it to be simple to understand and use day-to-day, especially by developers who may not have specific security expertise.
So, when you [enable GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/#configuration), it automatically detects the programming languages used in your project and runs the right security analyzers.

While [basic SAST scans](https://docs.gitlab.com/ee/user/application_security/sast/#summary-of-features-per-tier) are available in every GitLab tier, organizations that use GitLab SAST in their security programs should use Ultimate.
Only GitLab Ultimate [includes](https://docs.gitlab.com/ee/user/application_security/sast/#summary-of-features-per-tier):
- Proprietary technology for suppressing false positive results and tracking vulnerabilities as they move through codebases.
- Integrating SAST results into the merge request workflow.
- Vulnerability Management, Security Policies, and other capabilities that help you enforce security requirements.

### Context: Know our customers

The ideal/typical customer profile for GitLab SAST is affected by our pricing, our packaging (as part of Ultimate), and the market that our company addresses overall.
Customers approach SAST and other GitLab security products in a way that's somewhat different from other areas of the product.

#### Customer journey

There are at least a few big-picture aspects to mention:

- Our customers are generally trying to **bring developers into their security program**. They’re often doing this because there are so many more developers than security engineers. For our customers, security testing is not a "once in a release" task.
    - This applies at any organization size—we have customers with 50 developers and 1 security engineer, or thousands of developers and a team of dozens of security engineers. Notice how, in both cases, each security engineer has to scale their impact to many, many developers!
    - In many cases, these organizations are trying to get wide scanning coverage because their business has scaled faster than their security program.
    - As a result of these factors, our customers tend not to tolerate many false-positive (incorrect) results. They are (sometimes implicitly, sometimes explicitly) willing to accept some false-negative results (missed detections) so that they can avoid the false positives.
- Our customers generally tend to have **more modern technology stacks**. That’s because using GitLab is itself an indication that they’re at least _attempting_ something like DevOps, DevSecOps, or “digital transformation”.
    - This doesn’t mean that every customer is writing web applications and deploying them on Kubernetes or something like that—far from it. For example, we have a lot of hardware companies doing firmware development on C/C++ and banks or other entities building on what you could call “legacy” technologies.
    - However, this does mean that we can largely stick to more-modern languages and ecosystems as we develop features. This is part of why we currently are prioritizing improving result quality rather than expanding the set of languages scanned.
- Because of how GitLab is priced and packaged, our customers typically are **uptiering from Premium to Ultimate** when they have their first experience of GitLab SAST. It is comparatively rare for customers to purchase Ultimate right off the bat, though we want this to happen too.
    - Either way, during their evaluation, they are often in the midst of a time-limited Ultimate trial (typically 30 days).
    - If a customer is upgrading from Premium, they experience a list-price increase of roughly 3x. (The price difference an individual customer sees will vary depending on the terms of their existing contract.)
    - Customers often want to consolidate other products onto GitLab. These are often security tools because of the number of GitLab security features that are Ultimate-only.

#### Big-picture constraints

These large factors have led us to a few foundational design goals and constraints:

- **Developer-oriented:** Our customers are using GitLab SAST to scale up their AppSec programs. They need their developers to contribute to security improvements—or at least avoid making new security mistakes.
- **Fast:** Everyone's busy, and a security tool that doesn't deliver timely results is a security tool that's ignored. Results and guidance need to be available before the developer or security engineer switches to another task.
- **Deeply integrated:** GitLab is a unified DevSecOps platform, and our customers build their SDLC and processes around it. We can deliver uniquely convenient and "sticky" workflows as part of a platform.

#### Next-level details

Those foundational goals are helpful, but not specific enough to guide all decisions. Going one level deeper, we can use beliefs like the following:

- **False positives are disfavored:** Our customers tend to prefer fewer false-positive (incorrect) results. They are willing to tolerate some false-negative results (missed detections) so that they can avoid false positives.
- **We need to explain ourselves:** Our product serves a broad audience, from expert security engineers to junior developers. This means that we must explain potential vulnerabilities clearly—even if we raise a true-positive result, if the user is confused by it result and doesn't believe it's true, we don't get any credit for that!
- **Some results is better than none:** When a scan runs into an error, it's better to provide whatever insights we can correctly produce. We should still make the error visible in an appropriate way, but imagine a customer rolling out SAST to all their projects—it should "just work" and, if it doesn't work, this should not cause noisy pipeline failures or disrupt developer workflows.
- **Design as if scan results will be enforced automatically:** Development teams often do not take action unless they are required to, so our security customers often want to implement automated enforcement. They can't do this unless we are careful to design our findings to be enforceable—for example by giving enough metadata that a policy can target the most-reliable and highest-impact findings while not causing workflows to be disrupted for minor nitpicks or guesses.

### Strategy and themes
<!-- Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

The [customer context described above](#context-know-our-customers) informs our strategy.

#### Meet table-stakes requirements

We need to meet "table stakes" requirements because we aren't creating the market or introducing customers to the concept of a SAST product for the first time.
Nearly all of our customers have used a SAST product, or at least a related product like a linter or quality tool, before.

And, most organizations adopting Ultimate are going to compare us to other tools. Typically, they are either:

1. Trying to use GitLab SAST to **replace another product** they already use (usually paid, but sometimes open-source or free).
    - In this case they often know the incumbent product well. This can be good or bad, since customers will know both the benefits and the frustrations related to that tool!
    - The social/organizational dynamics are most interesting when one team (often Security) wants to keep the incumbent tool, but the other team (often Development) wants to adopt Ultimate. These head-to-heads can be particularly detailed and exacting.
    - Customers differ in how intensively they evaluate GitLab against their incumbent tools. They also differ in how quickly they expect to consolidate their other tools onto GitLab. Some do detailed evaluations as part of the initial evaluation, while others postpone this until closer to a renewal decision.
1. Evaluating which SAST tool they should **adopt for the first time**.
   - In this case we are usually compared against newer-generation, easier-to-purchase tools.

In both cases, customers will design some kind of evaluation or testing process, ideally in collaboration with their account team.
These can be very quantitative, comparing results and FP/FN rates, or they can be more qualitative.
Sometimes the evaluation is on a benchmark app like [DVWA](https://github.com/digininja/DVWA), [JuiceShop](https://github.com/juice-shop/juice-shop), or [OWASP Benchmark](https://owasp.org/www-project-benchmark/); evaluators often view these as "simple tests" so they expect GitLab SAST to perform well on them, even if they are unrepresentative of the organization's typical code.
Other times the evaluation is based on the organization's real code.

The upshot is that we need to perform _well enough_ in these evaluations to get through to the next stage of the evaluation.
In other words, our platform value proposition, or other features like security policies, don't get a chance to compete if we aren't at least _close_ in terms of result quality and clarity.

Similarly, we need to avoid being eliminated for lacking common "checklist" buyer features, like offering cross-file analysis, IDE integration, or ability to implement policies.

#### Lean into unique advantages

Often, the best products rely on some kind of "unfair advantage" against competitors. (For some companies, that's access to a brand-new technology; for others, it's the opportunity to start fresh without a legacy stack and customer base; for others, it's an uncommonly powerful sales and marketing motion; for others it's pricing or packaging.)

We will "punch above our weight" if we leverage GitLab's unique advantages. Those advantages include:

- Our product is a key part of user workflows throughout the SDLC: viewing source code, doing code review in merge requests, using the IDE, building artifacts, running tests, searching, planning, etc. We can build native integrations where other companies would not be able to.
- As a result, we have real-time access to data and events across the SDLC, such as commit history, builds, other information; many other tools do not have this data natively and must connect to it over APIs or other integrations.
- We are part of a unified platform that's prepaid annually, so we can lead customers and users on a gradual adoption or activation journey, progressively increasing the amount of insight and value we provide without requiring them to first buy a new product or pay a bill.

#### Focus on Ultimate, but support Free

GitLab SAST must exist in certain tiers based on our business model.

- **Ultimate:** We focus on Ultimate. Our main focus should be making Ultimate as valuable as we can. That's because security and compliance is a key driver of Ultimate revenue, and this is our role in GitLab's business.
- **All tiers:** "Basic scans" are currently available in all tiers.
    - This is partly due to our historical use of open-source technology and our desire to provide basic security hygiene to all. Today, the Free offering does not play a major part in our business strategy.
    - However, GitLab disfavors removing or uptiering features, so some basic GitLab SAST functionality must be available in Free/Premium.
    - This does not mean that some part of _every aspect of SAST_ is available in Free. For example, features like vulnerability management, vuln tracking, and UI flows are (and should remain) 100% Ultimate-only.
    - Similarly, proprietary technology in which we've invested significant resources will almost always remain Ultimate-only.

#### Don't invest uniformly

Not all features or languages are equal-value.

Our customers expect the features we have previously released to continue to exist, so we disfavor removing capabilities entirely.
This means that we should assume that we need to continue to maintain the support matrix and features we current have. For example, if we have declared support for scanning a particular language, we cannot easily remove it.

However, this _doesn't_ mean that we need to invest the same amount of effort or achieve the same sophistication for every language or feature.
For example, it is completely appropriate to continue to use open-source scanners to power less-commonly-used languages like Apex or Elixir until the appropriate customer demand shows us that we need to invest more resources in developing improved technology for those languages.

#### Operate everywhere

Our technology stack going forward must:

- Support operating in different contexts, including classic "shift left" contexts like the IDE and merge request (code review).
    - The biggest thing to remember is that we need to provide a _consistent_ experience across these contexts.
        - "Consistent" _does not_ mean "identical". For example, in the IDE we are subject to short time deadlines and our goal is to get easy-to-understand fixes in front of developers very quickly. In such cases we can make limitations like only scanning for simpler-to-detect issues (e.g. bad/risky functions) and reserve taint analysis for operational environments where longer-running scans are acceptable.
        - "Consistent" _does_ mean that the product feels like it's actually one product. So, for example, if you dismiss a finding in Vulnerability Management, that same finding should not reappear in the IDE. Or, if you disable a rule in a project, that preference should be applied everywhere that scanning happens.
- We also must support operating in all three types of GitLab Ultimate offerings: GitLab.com (multi-tenant SaaS), GitLab Dedicated (single-tenant SaaS), and self-managed.
    - Self-managed includes operating completely offline (not connected to GitLab cloud services in any way). A significant number of customers do this—often the most security-conscious ones, who also want to use SAST and other security tools.

#### Allow customization, but still be opinionated

Some customers want a product that "just works"—more of a black box than anything.
Others will want to customize scan behavior to detect custom problems, for example insecure use of an internal library, that would not be appropriate to ship in a global ruleset to all customers.
Roughly speaking, most customers are closer to the "black box" mentality; a smaller proportion of security teams prefer the customization approach—typically those who have a Security Engineering approach and build significant hands-on expertise within their teams.

One strength of GitLab as a product is that we offer many basic "primitives" that customers can use in creative ways to achieve their goals.
But, it is easy to allow a product to become very complicated and hard-to-use in the service of endless customization. GitLab's [configuration principles](/handbook/product/product-principles/#configuration-principles) include:

- Ensure a great experience by default.
- Encourage favorable behaviors by limiting configuration.
- Avoid configuration completely when possible.

We should be opinionated about the interfaces we offer, including the types of customizations we support, so that we can give customers simplicity and maintain our own technical flexibility.
For example, customers sometimes want to ban calls to particular libraries or functions.

- One way to offer this is to allow injection of fully custom rules.
- But another would be to accept, as a specific configuration option, a list of function names that should never be called.

The two options here differ greatly in:

- the amount of complexity surfaced to the user—learning a pattern language and a YAML format, for example, versus entering a function name into a form.
- how much the customization dictates other technology choices. While a generic list of banned functions could be consulted by many different scanning systems, a more generic pattern is unlikely to losslessly translate to systems other than the one it was originally designed for.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
 -->

GitLab Static Analysis and Vulnerability Research teams are collaborating to improve the customer experience with SAST.

We've identified four themes that summarize the work we're planning and completing:

1. **Result quality:** Security professionals and developers should be able to trust every result from GitLab SAST.
    - This is our most foundational initiative, because detection is the key task for any SAST product.
        - This work improves the user experience directly, but also _indirectly_ by reducing the pressure on other workflows, for example by reducing how often users must complete tasks like triaging and analyzing results.
    - Result quality depends on both the _detection engine_ and the _detection rules_ that specify how to detect security vulnerabillities using the program model the engine builds. We are investing in both of these areas.
1. **Shifting further left:** GitLab SAST already scans code as soon as it's pushed, before code reviews even begin. But, we can make it easier to improve security by scanning code even earlier, including before code leaves developer machines.
1. **"Day 1" experience and "Day 2" efficiency:** We need to be sure SAST is both easy to enable ("Day 1") and operate going forward ("Day 2").
1. **Triage and remediation experience:** We value our users' time, and we know that vulnerabilities are resolved more often if they're easier to understand and interact with.

#### Theme prioritization

⭐️ We're actively prioritizing these themes for feature implementation and delivery:

1. **Result quality:** We're currently working to:
    1. _Add cross-file, cross-function scanning._ We are doing this by integrating Oxeye's SAST engine, following our acquisition of Oxeye. We expect to deliver cross-file, cross-function scanning for Python, Go, Java, and JavaScript by the end of January 2025. This will be available as part of GitLab Ultimate.
    1. _Add and update detection rules._ We continue to invest in analyzing and changing our SAST ruleset. This includes updates to existing rules, development of new rules, and curation of community rules. We also will integrate Oxeye's detection rules into a combined ruleset.
1. **Shifting further left:**  We're currently working to expand [SAST scanning in IDEs](https://gitlab.com/groups/gitlab-org/-/epics/9679).
    - Today, you can see [CI/CD pipeline-based security scan results in the VS Code IDE](https://gitlab.com/groups/gitlab-org/-/epics/9004).
    - Next, we're working on [real-time SAST scanning in the IDE](https://gitlab.com/groups/gitlab-org/-/epics/10283), which scans code as developers are working.

✅ We're wrapping up work on this theme after GitLab 17.0:

1. **"Day 1" experience and "Day 2" efficiency:**
    - Our main focus in this area is making the scanning process simpler and more consistent across languages. We do this by consolidating scanning coverage from language-specific scanners to Semgrep-based scanning with [GitLab-managed rules](https://docs.gitlab.com/ee/user/application_security/sast/rules.html#source-of-rules). This provides a simpler, more consistent operational experience; allows GitLab to directly manage rules; and makes scans run faster.
    - GitLab 17.0 will include [a large number of analyzer consolidations](https://gitlab.com/groups/gitlab-org/-/epics/13050). After GitLab 17.0, we will shift our focus to the areas above.

🔮 We're focusing on design and discovery efforts, rather than immediate feature delivery, for this theme:

1. **Triage and remediation experience:** We value our users' time, and making it easier to _understand_ a potential vulnerability improves the chances it'll be resolved.
    - We recently added [SAST findings in the MR diff view](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#sast-results-in-mr-changes-view).
    - We are now looking for new opportunities to improve this experience, with an emphasis on smaller changes that have outsize impact.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next 3 months, we are planning to work on:
- **Result quality:**
    - **Engine:** Completing the first milestone for [integrating Oxeye-based cross-function, cross-file scanning](https://gitlab.com/groups/gitlab-org/-/epics/13278).
    - **Rules:** Continuing to [update the SAST detection rules](https://gitlab.com/groups/gitlab-org/-/epics/10907) that GitLab creates and maintains.
        - Our previous focus was on reducing false-positive results by removing low-value rules and updating detection logic.
        - We are now focusing more on adding new rules, including a curated set of community rules, to reduce false-negatives.
        - We will also be curating the ruleset for cross-file, cross-function scanning to match new engine capabilities.
- **Shifting further left:**
    -  We're focusing on [real-time SAST scanning in the IDE](https://gitlab.com/groups/gitlab-org/-/epics/10283), which scans code as developers are working.
        - We have completed a significant amount of technical research and analysis on this topic.
        - We will now work to design and build this feature, choosing and using appropriate technologies to analyze the code, run scans, and show results directly in the IDE's file editor view.

We will also wrap up work on:
- **"Day 1" experience and "Day 2" efficiency:**
    - Completing our effort to [consolidate language-specific analyzers in GitLab 17.0](https://gitlab.com/groups/gitlab-org/-/epics/13050).

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

We are currently working on:
- **Result quality:**
    - **Engine:** Completing the first milestone for [integrating Oxeye-based cross-function, cross-file scanning](https://gitlab.com/groups/gitlab-org/-/epics/13278).
    - **Rules:** Continuing to [update the SAST detection rules](https://gitlab.com/groups/gitlab-org/-/epics/10907) that GitLab creates and maintains.
        - Our previous focus was on reducing false-positive results by removing low-value rules and updating detection logic.
        - We are now focusing more on adding new rules, including a curated set of community rules, to reduce false-negatives.
        - We will also be curating the ruleset for cross-file, cross-function scanning to match new engine capabilities.
- **"Day 1" experience and "Day 2" efficiency:**
    - [Consolidating five language-specific analyzers](https://gitlab.com/groups/gitlab-org/-/epics/13050) into Semgrep-based scanning with GitLab-managed rules.
        - This change will be fully released in GitLab 17.0, including a [data migration](https://gitlab.com/gitlab-org/gitlab/-/issues/444926) to clean up no-longer-relevant findings.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

Our recent work includes:

- Introducing GitLab-managed rules for multiple programming languages in preparation for [consolidating analyzer coverage in GitLab 17.0](https://gitlab.com/groups/gitlab-org/-/epics/13050).
- Significant (and ongoing) [improvements to the default SAST ruleset](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#updated-sast-rules-to-reduce-false-positive-results), delivering substantially fewer false-positive and false-negative results in 16.8 and future releases. See [epic 10907](https://gitlab.com/groups/gitlab-org/-/epics/10907) for further progress.
- [Integrating SAST results into the MR diff view](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#sast-results-in-mr-changes-view) (in the Changes tab). This project involved updating the Code Quality inline diff feature and adding SAST results to it.
- [Improvements](https://gitlab.com/groups/gitlab-org/-/epics/11395) to the [automatic resolution of findings from removed rules](https://docs.gitlab.com/ee/user/application_security/sast/#automatic-vulnerability-resolution). This makes the rollout of rule removals significantly clearer for users.
- [More detailed security findings](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#more-detailed-security-findings-in-vs-code) in the recently released [integration of CI/CD pipeline-based security scan results into the VS Code IDE](https://gitlab.com/groups/gitlab-org/-/epics/9004).
- [Improvements to Advanced Vulnerability Tracking (AVT)](https://gitlab.com/groups/gitlab-org/-/epics/5144). We expanded AVT to all possible analyzers and improved the algorithm to handle more cases. These improvements were released iteratively during [16.2](https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#improved-sast-vulnerability-tracking), [16.3](https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/#improved-sast-vulnerability-tracking), and [16.4](https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/#improved-sast-vulnerability-tracking).

Check [older release posts](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features&selectedCategories=SAST&minVersion=13_00) for our previous work in this area.

#### What is not planned right now

We understand the value of many potential improvements to GitLab SAST, but aren't currently planning to work on the following initiatives:

- **Expanding language support:** Currently, we're focusing on delivering better-quality results, faster, for [the many languages and frameworks we already support](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks). We're not actively adding adding support for new languages.
However, if we don't support a language you use, you can:
    - [Integrate third-party tools](https://docs.gitlab.com/ee/development/integrations/secure.html) (open-source or proprietary) using our [documented, open report format](https://docs.gitlab.com/ee/development/integrations/secure.html#report).
    - Document your request by opening an issue or commenting on an existing issue in [epic 297](https://gitlab.com/groups/gitlab-org/-/epics/297).
- **Incremental scanning:** We're currently focusing on making _all_ scans faster rather than developing different types of scans for different cases, such as [incremental scans](https://gitlab.com/gitlab-org/gitlab/-/issues/419734) that scan only modified code. However, this is an area that we have researched and hope to address following the integration of the Oxeye SAST engine.

### What SAST is and isn't

SAST helps developers identify weaknesses and security issues early in the software development lifecycle, soon after code is written.

SAST _does_:

- Use static analysis techniques to find issues early in the development process.
- Identify weaknesses, which may be vulnerabilities, in the code it scans.
- Analyze the control and data flow of your program, check how functions are called, and otherwise analyze what the code could do at runtime.
- Help find issues that code reviewers or tests might miss.

SAST _doesn't_:

- Find known vulnerabilities in software dependencies; this is [software composition analysis](/direction/secure/composition-analysis/software-composition-analysis/).
- Run code and attempt to trigger unexpected behaviors; this is [dynamic analysis](/direction/secure/dynamic-analysis/).
- Look for generic bugs or maintenance issues; this is [Code Quality](/direction/secure/secret-detection/code_quality/).
- Replace code reviewers or tests; it augments them instead.

<!-- TODO: Add Best-in-Class Landscape section. -->
