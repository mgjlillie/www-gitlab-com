---
layout: markdown_page
title: "Category Direction - Wiki"
description: "GitLab Wikis are a great way to share documentation and organize information via built-in functionality. View further information here!"
canonical_path: "/direction/plan/knowledge/wiki/"
---

- TOC
{:toc}

## Wiki

| | |
| --- | --- |
| Stage | [Plan](/direction/plan/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-06-12` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting the Wiki category direction page in GitLab. This page belongs to [Knowledge group](/handbook/product/categories/#knowledge-group) in Plan Stage. This page is maintained by Knowledge group PM Matthew Macfarlane ([E-Mail](mailto:mmacfarlane@gitlab.com)). More information can be found on the [Knowledge group direction page](/direction/plan/knowledge/).

This page and associated strategy are a work in progress, and everyone can contribute to it:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3AWiki) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWiki) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Wiki, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

The GitLab Wiki is a great place to store documentation and organize information, and an alternative to tools like Confluence and Notion. Each GitLab project and group includes a Wiki.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LzFRBMGl2SA?start=541" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Where we are headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

We want the wiki to serve as a single source of truth for project and group-level documentation that is approachable and easily accessibly by anyone. As we look to future plans, we will be exploring ways to encourage collaboration across all personas by improving the editing and navigation experience.

Specifically, we are looking to better integrate the wiki experience with the rest of GitLab. One immediate opportunity is to better integrate with Issues and Epics.

### Target audience and experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/product/personas/#user-personas) involved in this category. An overview
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
One of our primary personas is Sasha, the [Software Developer](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer), but all personas can use wikis for storing information. Everyone should be able to quickly and easily contribute to a wiki page. As the wiki matures we may need to investigate other non-developer personas as our research found they are frequent users of wikis.

Another of our primary personas is Parker, the [Product Manager](https://handbook.gitlab.com/handbook/product/personas/#parker-product-manager). Product Managers require a place to develop, organize, and share their knowledge, and a wiki is often considered a perfect location. Leveraging templates, autocomplete connection of Wiki pages to GitLab Issues and Epics, Product Managers can seamlessly plan and develop their ideas using the GitLab wiki.

### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

We recently [finalized](https://gitlab.com/groups/gitlab-org/-/epics/12826) Jobs to Be Done (JTBD) for the category which help us to narrow down customer priorities. In 16.10 we released [templates for wiki](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133279), and in 16.11 we released [autocomplete feature](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133281) to connect Wiki pages to issues/epics.

### What is not planned right now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

In the past, even as of the March 2024 update, we covered extensively how we were not planning on working on collaborative editing. However, given the emphasis on this from users and insights from research, there is a clear need for collaborative editing within the platform. We've created a small POC as of 2024-04-15, and hope to begin testing this internally sometime in 17.x. There is still quite a bit of work to be done here, but we are moving toward a solution. The first place we would place collaborative editing would be within the Wiki.

### Jobs to Be Done 

A Job to be Done (JTBD) is a framework, or lens, for viewing products and solutions in terms of the jobs customers are trying to achieve. It's about understanding the goals that people want to accomplish. It lets us step back from our business and understand the objectives of the people we serve. It opens the door to innovation.

At GitLab, we have our own flavor of JTBD and use it throughout the design process, but most notably to:

- Define scope
- Validate direction
- Evaluate existing experiences
- Assess category maturity

JTBD come directly from research and customer conversations with those people who do the tasks/jobs we need to design for. Problem validation is one of the most effective ways to confidently inform the writing of a JTBD.

For wiki category we've identified 9 core JTBD after two research studies. The following [Epic](https://gitlab.com/groups/gitlab-org/-/epics/12826) provides additional details about the construction of and research behind Wiki JTBD.

| Job Performer                    | Description                                           |
| ------------------------ | -------------------------------------------------- | 
| Knowledge Consumer       | When I am writing code, I want to find knowledge related to common roadblocks, so that I can minimize the likelihood of a security incident and increase development efficiency.| 
| Knowledge Admin         | When I am onboarding a new colleague, I want to ensure they are provided the right view and edit permissions related to knowledge we have stored, so I can minimize the likelihood of a security incident or release of material non public information. | 
| Knowledge Documenter               | When I am conducting a retrospective, I want to document the situation in a manner consistent with other retrospectives, so I can ensure colleagues are easily able to understand the situation and path to resolution. |
| Knowledge Admin          | When I am reviewing knowledge my team is consuming, I want to review which assets are utilized the most, so I can ensure the content most digested is accurate and providing insights that create informed decisions.|
| Knowledge Consumer            | When I am examining open work items for my engineering team, I want to review the knowledge documented for each item, so I can understand the broader context around prioritization and background knowledge for resolution.|
| Knowledge Admin            | When I am tasked with importing knowledge from what tool to another tool, I want to efficiently view what knowledge has moved, so I can ensure that no knowledge was lost in transition. |
| Knowledge Consumer | When I am on call, I want to understand who created the documentation in the knowledge base, so I can easily reach out to them if the incident becomes critical.   |
| Knowledge Documenter | When I am reviewing a document, I want to understand who is actively contributing to the documentation, so I can ensure I do not overlap with their changes and am more efficient with my efforts.|
| Knowledge Documenter | When I am reviewing a Page I want to be able to comment my thoughts on improvements or ask questions related to the documentation, so I can improve my own efficiency and the efficiency of the organization.|

### Maturity plan

We are actively working on maturing the Wiki category. In the past the category was in Mainteance Mode, however, due to repriotization and a need for a competitive tool to compete against Confluence we have begun investing in this category. We recently completed advancements in our product via the introduction of templates and an autocomplete feature. We have plans to continue maturing the category, including completing [Uncoupling wiki page slugs, filename and title](https://gitlab.com/groups/gitlab-org/-/epics/3192) which is one of the most outstanding Issues with the Wiki.


### Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

We compete with the following knowledge management tools (not and exclusive list):

- [Confluence](https://www.atlassian.com/software/confluence)
- [Notion](https://www.notion.so/)
- [GitHub Wiki](https://docs.github.com/en/communities/documenting-your-project-with-wikis/about-wikis)

<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- TBD -->

### Recent Accomplishments

- In 16.11 we [released](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133281) autocomplete features for the wiki.
- In 16.10 we [released](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133279) template capabilities for the wiki. 
- In 16.6 we [released](https://gitlab.com/gitlab-org/gitlab/-/issues/422093) a print wiki page to PDF feature.
- In 15.10 we [released](https://gitlab.com/gitlab-org/gitlab/-/issues/20305/?_gl=1*17d6cyx*_ga*MTU5MDI5ODc5NS4xNjY1NTkyMzcy*_ga_ENFH3X7M5Y*MTY4MDcyMDIxOC4zOTUuMS4xNjgwNzIxMTIzLjAuMC4w) the capability to easily create and edit diagrams in wikis by using the diagrams.net GUI editor. This feature is available in the Markdown editor and the content editor, and was implemented in close collaboration with the GitLab wider community.

### Top customer success/sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Decouple wiki page slugs, filename, and titles](https://gitlab.com/groups/gitlab-org/-/epics/3192)
- [Improve AsciiDoc, RDoc and reStructuredText support in Wikis](https://gitlab.com/groups/gitlab-org/-/epics/701)
- [Improve wiki navigation](https://gitlab.com/groups/gitlab-org/-/epics/700)

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Make it possible to edit wiki through CI](https://gitlab.com/gitlab-org/gitlab/-/issues/16261) (195 Upvotes)

### Top dogfooding issues
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

GitLab does not have any organization-wide wikis, but some teams do use them for various purposes.

[Knowledge group utilizes the GitLab Wiki](https://gitlab.com/gitlab-org/plan-stage/knowledge-group/-/wikis/Knowledge-Group-Home) for Opportunity Mapping via the recent introduction of diagrams.net. More information can be found on this integration via our Unfiltered channel.

### Top strategy item(s)
<!-- What's the most important thing to move your strategy forward?-->

Our top strategy item, as of 2024-06-12, is to continue addressing the identified JTBD for the category. This includes wiki [advanced page permissions](https://gitlab.com/groups/gitlab-org/-/epics/12832), [page analytics](https://gitlab.com/groups/gitlab-org/-/epics/12834), improved [Confluence importer](https://gitlab.com/groups/gitlab-org/-/epics/12830), [real time collaboration](https://gitlab.com/groups/gitlab-org/-/epics/12828), and [page comments](https://gitlab.com/groups/gitlab-org/-/epics/12827). 
