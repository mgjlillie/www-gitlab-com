# https://docs.gitlab.com/ee/user/project/code_owners.html

# Each CODEOWNERS entry requires at least 2 or more individuals OR at least 1 group with access to the www-gitlab-com repository.

# The order in which the paths are defined is significant: the last pattern that matches a given path will be used to find the code owners.

[CODEOWNERS]
.gitlab/CODEOWNERS @laurenbarker @timzallmann @david @gitlab-com/office-of-the-ceo

# NB Do not move this entry from the top of this file

^[Default Description Templates]
.gitlab/issue_templates/Default.md @sytses @gitlab-com/office-of-the-ceo
.gitlab/merge_request_templates/Default.md @sytses @gitlab-com/office-of-the-ceo

# The handbook has now been migrated
^[Handbook]
/sites/handbook/source/handbook/ @bmarnane

^[Active Tests]
/sites/uncategorized/source/pricing/ @akramer

^[Team Page & Pets]
/data/team_members/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/images/team/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/company/team-pets/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov

[Tech Stack]
/data/tech_stack.yml @NabithaRao @marc_disabatino

^[Compensation]
/data/currency_conversions.yml @wendybarnes @mwilkins
/data/entity_mapper.yml @wendybarnes @anechan
/data/country_payment_information.yml @wendybarnes @brobins
/data/variable_pay_frequency.yml @mwilkins

^[Engineering]
/data/projects.yml @joergheilig
/data/schemas/team_member.schema.json @joergheilig @meks @rymai

^[Learn at gitlab]
/source/images/learn/ @cmestel

^[Marketing]
/data/analyst_reports.yml @akramer
/data/redirects.yml @akramer
/data/webcasts.yml @aoetama
/source/images/heroes/ @johncoghlan
/sites/uncategorized/source/analysts/ @akramer
/sites/uncategorized/source/why/ @akramer
/sites/uncategorized/source/community/ @esalvadorp @akramer @nick_vh
/sites/uncategorized/source/community/contribute/ @esalvadorp @nick_vh @daniel-murphy
/sites/uncategorized/source/community/contribute/dco-cla/ @esalvadorp @dfrhodes @nick_vh
/sites/uncategorized/source/community/evangelists/ @esalvadorp @johncoghlan
/sites/uncategorized/source/community/gitlab-first-look/ @asmolinski2
/sites/uncategorized/source/community/hackathon/ @esalvadorp @johncoghlan @nick_vh @leetickett-gitlab @daniel-murphy
/sites/uncategorized/source/community/heroes/ @esalvadorp @johncoghlan
/sites/uncategorized/source/community/issue-bash/ @meks @nick_vh
/sites/uncategorized/source/community/meetups/ @esalvadorp @johncoghlan
/sites/uncategorized/source/community/mvp/ @esalvadorp @nick_vh
/sites/uncategorized/source/community/ten-year/ @esalvadorp @johncoghlan
/sites/uncategorized/source/community/top-annual-contributors/ @esalvadorp @nick_vh
/sites/uncategorized/source/community/virtual-meetups/ @johncoghlan
/sites/uncategorized/source/includes/contact-sales.html.haml @akramer
/sites/uncategorized/source/resources/ @aoetama
/sites/**/core-team/ @leetickett-gitlab @nick_vh

^[Marketing Community]
/data/heroes.yml @esalvadorp @johncoghlan
/data/heroes_contributions.yml @esalvadorp @johncoghlan
/data/speakers.yml @johncoghlan @abuango
/data/speakers_requirements.yml @johncoghlan @abuango

^[Product]
/data/categories.yml @david @gitlab-da @esalvadorp 
/data/sections.yml @david @joergheilig
/data/stages.yml @david @joergheilig
/source/direction/ @david

^[Product - Dev Section]
/source/direction/dev/ @david @gl-product-leadership
/source/direction/dev/strategies/ @david @gl-product-leadership
/source/direction/manage/ @joshlambert @david @gl-product-leadership
/source/direction/manage/personal_productivity/ @david @joshlambert @gl-product-leadership
/source/direction/manage/import_and_integrate/ @david @joshlambert @m_frankiewicz @gl-product-leadership
/source/direction/plan/devops-reports/ @hsnir1 @david @gl-product-leadership
/source/direction/manage/code-analytics/ @hsnir1 @david @gl-product-leadership
/source/direction/manage/personal_productivity/notifications/ @gl-product-leadership @joshlambert @david
/source/direction/manage/personal_productivity/navigation/ @gl-product-leadership @joshlambert @david
/source/direction/manage/personal_productivity/settings/ @gl-product-leadership @joshlambert @david
/source/direction/manage/design_system/ @gl-product-leadership @joshlambert @david
/source/direction/manage/personal_productivity/gitlab_docs/ @gl-product-leadership @joshlambert @david
/source/direction/plan/ @mushakov @david @gl-product-leadership
/source/direction/plan/value_stream_management/ @hsnir1 @david @gl-product-leadership
/source/direction/plan/portfolio_management/ @mushakov @david @amandarueda @gl-product-leadership
/source/direction/plan/design_management/ @mushakov @david @amandarueda @gl-product-leadership
/source/direction/plan/project_management/ @mushakov @david @gweaver @gl-product-leadership
/source/direction/plan/project_management/team_planning/ @mushakov @david @gweaver @gl-product-leadership
/source/direction/plan/knowledge/content_editor/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/wiki/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/pages/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/create/ @derekferguson @david @gl-product-leadership
/source/direction/create/source_code_management/ @david @derekferguson @mcbabin @gl-product-leadership
/source/direction/create/code_review_workflow/ @derekferguson @david @phikai @gl-product-leadership
/source/direction/create/ide/web_ide/ @derekferguson @david @gl-product-leadership
/source/direction/create/code_creation/code_suggestions/ @derekferguson @kbychu @hbenson @gl-product-leadership
/source/direction/create/editor_extensions/ @derekferguson @dashaadu @david @hbenson @phikai @gl-product-leadership
/source/direction/create/gitlab_cli/ @derekferguson @david @phikai @gl-product-leadership
/source/direction/create/source_code_management/source_editor/ @derekferguson @mcbabin @david @gl-product-leadership
/source/direction/create/gitter/ @derekferguson @david @gl-product-leadership

^[Product Section - Ops]
/source/direction/ops/ @david @mflouton
/source/direction/delivery/ @nagyv-gitlab
/source/direction/package/ @trizzi
/source/direction/verify/ @jreporter
/source/direction/ci/ @jreporter
/source/direction/verify/code_testing/ @jocelynjane @jreporter
/source/direction/verify/build_artifacts/ @jocelynjane @jreporter
/source/direction/verify/performance_testing/ @jocelynjane @jreporter
/source/direction/verify/review_apps/ @jocelynjane @jreporter
/source/direction/verify/hosted_runners/ @jreporter @gabrielengel_gl

^[Product Section - Sec]
/source/direction/security/ @hbenson @gl-product-leadership
/source/direction/secure/ @sarahwaldner @hbenson
/source/direction/secure/static-analysis/ @connorgilbert @sarahwaldner
/source/direction/secure/secret-detection/ @smeadzinger @sarahwaldner
/source/direction/secure/dynamic-analysis/ @smeadzinger @sarahwaldner
/source/direction/secure/vulnerability-research/ @sarahwaldner
/source/direction/secure/composition-analysis/ @johncrowley @sarahwaldner
/source/direction/govern/ @sam.white @hbenson @g.hickman @abellucci @hsutor @jrandazzo
/source/direction/govern/threat_insights/ @abellucci @sam.white @hbenson
/source/direction/govern/security_policies/ @g.hickman @sam.white @hbenson
/source/direction/govern/compliance/ @g.hickman @sam.white @hbenson
/source/direction/govern/anti-abuse/ @sam.white @hbenson
/source/direction/govern/authentication/ @hsutor @sam.white @hbenson
/source/direction/govern/authorization/ @jrandazzo @sam.white @hbenson
/source/direction/supply-chain/ @sam.white @hbenson

^[Product - Analytics Section]
/source/direction/monitor/ @david @justinfarris @stkerr
/source/direction/monitor/analytics-instrumentation/ @david @justinfarris @stkerr @tjayaramaraju
/source/direction/monitor/product-analytics/ @david @justinfarris @stkerr
/source/direction/monitor/observability/ @sguyon

^[Product - SaaS Platforms Section]
/source/direction/saas-platforms/ @fzimmer @david @gl-product-leadership
/source/direction/saas-platforms/delivery/ @fzimmer @swiskow
/source/direction/saas-platforms/scalability/ @fzimmer @swiskow
/source/direction/saas-platforms/dedicated/ @fzimmer @cbalane
/source/direction/saas-platforms/switchboard/ @fzimmer @lbortins

^[Product - Core Platform Section]
/source/direction/core_platform/ @joshlambert @david @gl-product-leadership
/source/direction/core_platform/tenant-scale/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/cell/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/organization/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/groups-&-projects/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/user-profile/ @joshlambert @lohrc
/source/direction/geo/ @joshlambert @sranasinghe
/source/direction/geo/geo_replication/ @joshlambert @sranasinghe
/source/direction/geo/backup_restore/ @joshlambert  @sranasinghe
/source/direction/geo/disaster_recovery/ @joshlambert @sranasinghe
/source/direction/global-search/ @joshlambert @bvenker
/source/direction/cloud-connector/ @joshlambert @rogerwoo
/source/direction/distribution/ @joshlambert @dorrino
/source/direction/core_platform/dotcom/ @joshlambert
/source/direction/database/ @joshlambert
/source/direction/gitaly/ @joshlambert @mjwood

^[Product - Fulfillment Section]
/source/direction/fulfillment/ @ofernandez2
/source/direction/fulfillment/consumables-cost-management/ @alex_martin
/source/direction/fulfillment/seat-cost-management/ @alex_martin
/source/direction/fulfillment/subscription-management/ @tgolubeva
/source/direction/fulfillment/fulfillment-admin-tooling/ @ppalanikumar
/source/direction/fulfillment/customers-dot-application/ @ppalanikumar
/source/direction/fulfillment/fulfillment-infrastructure/ @ppalanikumar
/sites/uncategorized/source/pricing/licensing-faq/cloud-licensing/ @courtmeddaugh

^[Product Section - Data Science]
/source/direction/data-science/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/modelops/ @kbychu @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ai_framework/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/duo_chat/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/ai_model_validation/ @tmccaslin @hbenson @gl-product-leadership

^[Product Section - Service Management]
/source/direction/service_management/ @hbenson

^[Product Section - Other]
/source/direction/product-analysis/ @cbraza @gl-product-leadership
/source/direction/mobile/mobile-devops/ @darbyfrey @bmarnane @meks
/data/jtbd.yml @andyvolpe @jmandell

^[Recruiting]
/sites/uncategorized/source/jobs/ @rallen3 @drogozinski

^[Security Compliance]
/sites/uncategorized/source/security/ @joshlemos @jlongo_gitlab

^[Releases blog posts]
/sites/uncategorized/source/releases/posts/ @gitlab-org/delivery, @gl-product-leadership

^[Delivery team]
/data/releases.yml @gitlab-org/delivery

^[Speakers Bureau]
/sites/uncategorized/source/speakers/ @abuango @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/index.html.erb @abuango @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/data.json.erb @abuango @johncoghlan @dnsmichi
/data/speakers.yml @abuango @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/ @abuango @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/SpeakersBureau.vue @abuango @johncoghlan @dnsmichi

^[Environment Setup Script]
/scripts/setup-macos-dev-environment.sh @cmestel

^[Ruby Version]
.ruby-version @laurenbarker @cwoolley-gitlab

# This is the Legal and Corporate Affairs section.
[Legal and Corporate Affairs Documents]
/sites/uncategorized/source/privacy/cookies/index.html.md @robin @ktesh @m_taylor @gitlab-com/egroup
/sites/uncategorized/source/terms/signature.html.md @robin @ktesh @m_taylor @gitlab-com/egroup
